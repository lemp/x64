#!/bin/sh

set -e

if [ -e /.init ]; then
	exit
fi

touch /.init

mkdir -p /config/mariadb /config/nginx /var/www /var/www/html

DB_DIR=${DB_DIR:-/databases}

if [ ! -s /config/mariadb/my.cnf ]; then
	cat << 'EOF' > /config/mariadb/my.cnf
[mysqld_safe]
nice				= 0

[client]
port  		        	= 3306
socket          		= /run/mysqld/mysqld.sock

[mysqld]
socket          		= /run/mysqld/mysqld.sock
port            		= 3306
skip-external-locking
skip-name-resolve

max_connections			= 20
key_buffer_size                 = 16M
max_allowed_packet              = 1M
table_open_cache                = 64
sort_buffer_size                = 512K
net_buffer_length               = 8K
read_buffer_size                = 256K
read_rnd_buffer_size            = 512K
myisam_sort_buffer_size         = 8M

default_storage_engine		= InnoDB
innodb_buffer_pool_size		= 16M
innodb_additional_mem_pool_size	= 2M
innodb_log_file_size    	= 5M
innodb_log_buffer_size		= 8M
innodb_flush_log_at_trx_commit  = 1
innodb_lock_wait_timeout        = 50
slow_query_log_file		= /databases/mariadb-slow.log
long_query_time			= 10
log_slow_verbosity		= query_plan
log_error 			= /databases/error.log
log_bin				= /databases/mariadb-bin
log_bin_index			= /databases/mariadb-bin.index
expire_logs_days		= 7
max_binlog_size         	= 10M

character-set-server		= utf8
collation-server		= utf8_general_ci

[mysqldump]
quick
quote-names
max_allowed_packet		= 16M

[myisamchk]
key_buffer_size                 = 20M
sort_buffer_size                = 20M
read_buffer                     = 2M
write_buffer                    = 2M

[mysqlhotcopy]
interactive-timeout
EOF

	if [ "$(realpath /etc/mysql/my.cnf)" != "/config/mariadb/my.cnf" ]; then
		ln -sf /config/mariadb/my.cnf /etc/mysql/my.cnf
	fi
fi

if [ ! -s /config/nginx/nginx.conf ]; then
	cat << 'EOF' > /config/nginx/nginx.conf
user nginx;

# Set number of worker processes automatically based on number of CPU cores.
worker_processes auto;

# Enables the use of JIT for regular expressions to speed-up their processing.
pcre_jit on;

# Configures default error logger.
error_log /dev/stdout error;

# Includes files with directives to load dynamic modules.
include /etc/nginx/modules/*.conf;


events {
	# The maximum number of simultaneous connections that can be opened by
	# a worker process.
	worker_connections 1024;
}

http {
	# Includes mapping of file name extensions to MIME types of responses
	# and defines the default type.
	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	# Name servers used to resolve names of upstream servers into addresses.
	# It's also needed when using tcpsocket and udpsocket in Lua modules.
	#resolver 208.67.222.222 208.67.220.220;

	# Don't tell nginx version to clients.
	server_tokens off;

	# Specifies the maximum accepted body size of a client request, as
	# indicated by the request header Content-Length. If the stated content
	# length is greater than this size, then the client receives the HTTP
	# error code 413. Set to 0 to disable.
	client_max_body_size 1m;

	# Timeout for keep-alive connections. Server will close connections after
	# this time.
	keepalive_timeout 65;

	# Sendfile copies data between one FD and other from within the kernel,
	# which is more efficient than read() + write().
	sendfile on;

	# Don't buffer data-sends (disable Nagle algorithm).
	# Good for sending frequent small bursts of data in real time.
	tcp_nodelay on;

	# Path of the file with Diffie-Hellman parameters for EDH ciphers.
	#ssl_dhparam /etc/ssl/nginx/dh2048.pem;

	# Specifies that our cipher suits should be preferred over client ciphers.
	ssl_prefer_server_ciphers on;

	# Enables a shared SSL cache with size that can hold around 8000 sessions.
	ssl_session_cache shared:SSL:2m;

	# Set the Vary HTTP header as defined in the RFC 2616.
	gzip_vary on;

	# Specifies the main log format.
	log_format main '$remote_addr - $remote_user [$time_local] "$request" '
			'$status $body_bytes_sent "$http_referer" '
			'"$http_user_agent" "$http_x_forwarded_for"';

	# Sets the path, format, and configuration for a buffered log write.
	access_log /var/log/nginx/access.log main;

        # Includes virtual hosts configs.
        server {
                listen          80;

                server_name     localhost;

                root    /var/www/html;
                index   index.php dashboard.php;

                location ~ \.php$ {
                        try_files       $uri =404;
                        fastcgi_pass    127.0.0.1:9000;
                        fastcgi_index   index.php;
                        include         /etc/nginx/fastcgi.conf;
                }
        }
}
EOF
fi

chown -R mysql:mysql ${DB_DIR}
chmod 2755 ${DB_DIR}

if [ ! -e ${DB_DIR}/ibdata1 ]; then
	rm -rf ${DB_DIR}/*

	mysql_install_db --user=mysql --basedir=/usr --datadir=${DB_DIR}
	mysqld_safe --user=mysql --datadir=${DB_DIR} --nowatch
	mysqld_pid=
	for i in $(seq 60)
	do
		mysqld_pid="$(ps -ef | grep mysqld | grep -v grep | sed -e "s/^\s*\([[:digit:]]\+\)\s\+.*$/\1/g")"
		if [ -e /run/mysqld/mysqld.sock -a ! -z "${mysqld_pid}" ]; then
			break
		fi
		sleep 1
	done
	if [ -z "${mysqld_pid}" ]; then
		echo "mysqld_safe start failed"
		exit 1
	fi
	mysqladmin -u root password passwd
	kill -s TERM ${mysqld_pid}
	for i in $(seq 60)
	do
		mysqld_pid="$(ps -ef | grep mysqld | grep -v grep | sed -e "s/^\s*\([[:digit:]]\+\)\s\+.*$/\1/g")"
		if [ ! -e /run/mysqld/mysqld.sock -a -z "${mysqld_pid}" ]; then
			break
		fi
		sleep 1
	done
	if [ ! -z "${mysqld_pid}" ]; then
		echo "mysqld_safe stop failed"
		exit 1
	fi
	cat << EOF > /usr/local/etc/s6/mysqld/run
#!/bin/sh

exec mysqld_safe --user=mysql --datadir=${DB_DIR}
EOF
	chmod 755 /usr/local/etc/s6/mysqld/run
fi
